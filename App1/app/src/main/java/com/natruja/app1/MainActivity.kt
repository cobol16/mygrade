package com.natruja.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    var btn1: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    btn1 = findViewById(R.id.btn)

        btn1?.setOnClickListener{
            val intent = Intent(this,HelloActivity::class.java)
                startActivity(intent)
        }
    }
}