       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READE-GRADE-TO-AVG-GRAD.
       AUTHOR. NATRUJA.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT AVG-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "avg-grade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  AVG-FILE.
       01  AVG-DDETAIL.
           88  END-OF-AVG-FILE VALUE  HIGH-VALUE .
           05 SUB-ID    PIC   9(6).
           05 SUB-NAME  PIC   X(50).
           05 CREDIT    PIC   9.
           05 GRADE     PIC   X(2).
       FD  AVG-GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-AVG-GRADE-FILE VALUE  HIGH-VALUE .
           05 AVG-GRADE          PIC 9V9(3).
           05 AVG-SCI-GRADE      PIC 9V9(3).
           05 AVG-CS-GRADE       PIC 9V9(3).
       WORKING-STORAGE SECTION. 
       01  GRADE1             PIC 9(3)V9(3).
       01  SUM-CREDIT         PIC 9(3)V9(3).
       01  SUM-GRADE          PIC 9(3)V9(3).
       01  SUM-GRADE1         PIC 9(3)V9(3).
       01  SCI-SUM            PIC 9(3)V9(3).
       01  SCI-CREDIT         PIC 9(3)V9(3).
       01  SCI-GRADE          PIC 9(3)V9(3).
       01  CS-SUM             PIC 9(3)V9(3).
       01  CS-CREDIT          PIC 9(3)V9(3).
       01  CS-GRADE           PIC 9(3)V9(3).
       01  SUB                PIC X.
       01  SUB1               PIC X(2).
       PROCEDURE DIVISION .
       000-BEGIN.
           OPEN INPUT AVG-FILE 
           OPEN OUTPUT  AVG-GRADE-FILE 
           PERFORM  UNTIL END-OF-AVG-FILE
              READ AVG-FILE 
                 AT END SET END-OF-AVG-FILE TO TRUE
              END-READ
              IF NOT END-OF-AVG-FILE THEN
                 MOVE SUB-ID  TO  SUB 
                 MOVE SUB-ID  TO  SUB1
                 PERFORM  001-PROCESS   THRU  001-EXIT
              END-IF 
           COMPUTE  AVG-GRADE      = SUM-GRADE1  / SUM-CREDIT   
           COMPUTE  AVG-SCI-GRADE  = SCI-GRADE  / SCI-CREDIT   
           COMPUTE  AVG-CS-GRADE      = CS-GRADE  / CS-CREDIT   
           END-PERFORM
           DISPLAY "AVG-GRADE " AVG-GRADE
           DISPLAY "AVG-SCI-GRADE " AVG-SCI-GRADE 
           DISPLAY "AVG-CS-GRADE " AVG-CS-GRADE
           WRITE  GRADE-DETAIL
           CLOSE  AVG-FILE 
           CLOSE  AVG-GRADE-FILE 
           GOBACK .
       001-PROCESS. 
           EVALUATE  GRADE
              WHEN   "A"   MOVE 4   TO GRADE1
              WHEN   "B+"  MOVE 3.5 TO GRADE1 
              WHEN   "B"   MOVE 3   TO GRADE1
              WHEN   "C+"  MOVE 2.5 TO GRADE1
              WHEN   "C"   MOVE 2   TO GRADE1
              WHEN   "D+"  MOVE 1.5 TO GRADE1
              WHEN   "D"   MOVE 1   TO GRADE1 
              WHEN OTHER               MOVE 0   TO GRADE1
           END-EVALUATE
           COMPUTE  SUM-CREDIT  = SUM-CREDIT  + CREDIT 
           COMPUTE  SUM-GRADE1   = SUM-GRADE1 + (GRADE1 * CREDIT )
           IF  SUB EQUAL 3 THEN 
           COMPUTE  SCI-CREDIT  = SCI-CREDIT  + CREDIT 
           COMPUTE  SCI-GRADE   = SCI-GRADE + (GRADE1 * CREDIT )
           END-IF  
           IF  SUB1 EQUAL 31 THEN 
           COMPUTE  CS-CREDIT  = CS-CREDIT  + CREDIT 
           COMPUTE  CS-GRADE   = CS-GRADE + (GRADE1 * CREDIT )
           END-IF
           .
       001-EXIT.
           EXIT.

       
           
      

       