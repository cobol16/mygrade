       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR.  NATRUJA.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION   IS LINE  SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD  AVG-FILE.
       01  AVG-DDETAIL.
           05 SUB-ID    PIC   9(6).
           05 SUB-NAME  PIC   X(50).
           05 CREDIT    PIC   9.
           05 GRADE     PIC   X(2).
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT AVG-FILE 
           MOVE  "202101"  TO SUB-ID
           MOVE  "Information Services and Study Fundamentals" 
                           TO SUB-NAME 
           MOVE  "2"       TO CREDIT 
           MOVE  "B+"      TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "212101"       TO SUB-ID
           MOVE  "English I "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "302111"       TO SUB-ID
           MOVE  "Calculus and Analytic Geometry I "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "303101"       TO SUB-ID
           MOVE  "Chemistry I  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.


           MOVE  "303102"       TO SUB-ID
           MOVE  "Chemistry Laboratory I "   TO SUB-NAME 
           MOVE  "1"            TO CREDIT 
           MOVE  "A"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "306101"       TO SUB-ID
           MOVE  "General Biology I "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "306102"       TO SUB-ID
           MOVE  "General Biology Laboratory I "   TO SUB-NAME 
           MOVE  "1"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.


           MOVE  "308101"       TO SUB-ID
           MOVE  "Introductory Physics I  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL. 

           MOVE  "308102"       TO SUB-ID
           MOVE  "Introductory Physics Laboratory  "   TO SUB-NAME 
           MOVE  "1"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL. 

           MOVE  "212102"       TO SUB-ID
           MOVE  "English II   "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL. 

           MOVE  "302112"       TO SUB-ID
           MOVE  "Calculus and Analytic Geometry II  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "302112"       TO SUB-ID
           MOVE  "Chemistry II  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "303104"       TO SUB-ID
           MOVE  "Chemistry Laboratory II   "   TO SUB-NAME 
           MOVE  "1"            TO CREDIT 
           MOVE  "B+"           TO GRADE 
           WRITE AVG-DDETAIL. 

           
           MOVE  "306103"       TO SUB-ID
           MOVE  "General Biology II   "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL.


           MOVE  "306104"       TO SUB-ID
           MOVE  "General Biology Laboratory II   "   TO SUB-NAME 
           MOVE  "1"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "308103"       TO SUB-ID
           MOVE  "Introductory Physics II   "   TO SUB-NAME 
           MOVE  "1"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "308104"       TO SUB-ID
           MOVE  "Introductory Physics Laboratory II    "   TO SUB-NAME 
           MOVE  "1"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310201"       TO SUB-ID
           MOVE  "Computer and Data Processing  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "A"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "203100"       TO SUB-ID
           MOVE  "Man and Civilization  "   TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "212301"       TO SUB-ID
           MOVE  "Reading Laboratory I  "   TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "226100"       TO SUB-ID
           MOVE  "Man and Economy   "   TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "302213"       TO SUB-ID
           MOVE  "Calculus and Analytic Geometry III   "   TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "D+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "302323"       TO SUB-ID
           MOVE  "Linear Algebra I  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "D"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310211"       TO SUB-ID
           MOVE  "Computer Science I   "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310212"       TO SUB-ID
           MOVE  "Discrete Mathematics  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "312201"       TO SUB-ID
           MOVE  "Elementary Statistics   "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "208101"       TO SUB-ID
           MOVE  "Thai I  "     TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "212302"       TO SUB-ID
           MOVE  "Reading Laboratory II  "   TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "224101"       TO SUB-ID
           MOVE  "Man and Politics  "   TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "225100"       TO SUB-ID
           MOVE  "Man, Society and Culture  "  TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "302315"       TO SUB-ID
           MOVE  "Ordinary Differential Equations  "  TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310213"       TO SUB-ID
           MOVE  "Computer Science II  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "B+"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310231"       TO SUB-ID
           MOVE  "Computer Oganization and Assembly Language   "   
                                TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "A"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310341"       TO SUB-ID
           MOVE  "Data Structure and Algorithms  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "441101"       TO SUB-ID
           MOVE  "Wellness Development  "   TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "601101"       TO SUB-ID
           MOVE  "Man and Aesthetics   "   TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "226111"       TO SUB-ID
           MOVE  "Introduction to Economics   "   TO SUB-NAME 
           MOVE  "2"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "230416"       TO SUB-ID
           MOVE  "Production Management  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "D"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "308371"       TO SUB-ID
           MOVE  "Introduction to Electronics  "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C+"           TO GRADE 
           WRITE AVG-DDETAIL.
           
           MOVE  "310332"       TO SUB-ID
           MOVE  "Computer Architecture   "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310444"       TO SUB-ID
           MOVE  "File Processing   "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "A"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310482"       TO SUB-ID
           MOVE  "Computer Graphic   "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "B"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "301301"       TO SUB-ID
           MOVE  "Quality Management   "   TO SUB-NAME 
           MOVE  "1"            TO CREDIT 
           MOVE  "D"           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310327"       TO SUB-ID
           MOVE  "Principles of Programming Languages   "   TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "A"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310342"           TO SUB-ID
           MOVE  "Database Design"  TO SUB-NAME 
           MOVE  "3"                TO CREDIT 
           MOVE  "B"                TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310414"                      TO SUB-ID
           MOVE  "Software Engineering   "     TO SUB-NAME 
           MOVE  "3"                           TO CREDIT 
           MOVE  "B"                           TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310453"                TO SUB-ID
           MOVE  "Operating System    "  TO SUB-NAME 
           MOVE  "3"                     TO CREDIT 
           MOVE  "A"                     TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310461"                   TO SUB-ID
           MOVE  "Data Communication    "   TO SUB-NAME 
           MOVE  "3"                        TO CREDIT 
           MOVE  "C+"                       TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "441113"             TO SUB-ID
           MOVE  "Basketball I    "   TO SUB-NAME 
           MOVE  "1"                  TO CREDIT 
           MOVE  "A"                  TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310452"                      TO SUB-ID
           MOVE  "Compiler Construction    "   TO SUB-NAME 
           MOVE  "3"                           TO CREDIT 
           MOVE  "B+"                          TO GRADE 
           WRITE AVG-DDETAIL.
           
           MOVE  "310462"                   TO SUB-ID
           MOVE  "Computer Networks    "    TO SUB-NAME 
           MOVE  "3"                        TO CREDIT 
           MOVE  "A"                        TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310481"                         TO SUB-ID
           MOVE  "Systems Analysis and Design"    TO SUB-NAME 
           MOVE  "3"                              TO CREDIT 
           MOVE  "C"                              TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310484"       TO SUB-ID
           MOVE  "Introduction to Artificial Intelligence    "  
                                TO SUB-NAME 
           MOVE  "3"            TO CREDIT 
           MOVE  "C"            TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310485"                         TO SUB-ID
           MOVE  "Office Automation Management"   TO SUB-NAME 
           MOVE  "3"                              TO CREDIT 
           MOVE  "C+"                             TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310492"                   TO SUB-ID
           MOVE  "Computer Seminar    "     TO SUB-NAME 
           MOVE  "1"                        TO CREDIT 
           MOVE  "B+"                       TO GRADE 
           WRITE AVG-DDETAIL.

           MOVE  "310417"             TO SUB-ID
           MOVE  "Selected Topics"    TO SUB-NAME 
           MOVE  "3"                  TO CREDIT 
           MOVE  "B"                  TO GRADE 
           WRITE AVG-DDETAIL.
           
           MOVE  "310491"                         TO SUB-ID
           MOVE  "Projects in Computer Science"   TO SUB-NAME 
           MOVE  "2"                              TO CREDIT 
           MOVE  "A"                              TO GRADE 
           WRITE AVG-DDETAIL.
           CLOSE AVG-FILE  
           GOBACK 
           .
           
